var fs = require('fs');

const chalk = require('chalk');

const { auth } = require('express-openid-connect');

const config = {
  required: false,
  auth0Logout: true,
  appSession: {
    secret: 'a long, randomly-generated string stored in env'
  },
  baseURL: 'http://localhost:4001',
  clientID: '42wjoMKaQ2rjNdYtqXkN14fTzQ9Q7NH8',
  issuerBaseURL: 'https://dev-wf0qsogs.eu.auth0.com'
};


var express = require('express'),
    app = express(),
    port = process.env.PORT || 4001,
    bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/routes/authRoutes');

routes(app); //register the route

// auth router attaches /login, /logout, and /callback routes to the baseURL
app.use(auth(config));



 app.listen(port,function(){
    console.log(`Authentication API is running on port ${ port }`);
 });

 // req.isAuthenticated is provided from the auth router
app.get('/', (req, res) => {
    res.send(req.isAuthenticated() ? 'Logged in' : 'Logged out');
  });
  
